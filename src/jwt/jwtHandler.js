const jwt = require('jsonwebtoken')
const fs = require('fs')
const { JWT_PRIVATE_KEY_PATH, JWT_PUBLIC_KEY_PATH } = require('../configuration/constants').security
const jwtGlobalOpts = require('../configuration/constants').jwt

const createJWT = (payload, options) => {
  const cert = fs.readFileSync(`${__dirname}${JWT_PRIVATE_KEY_PATH}`)

  const jwtOptions = {
    ...jwtGlobalOpts,
    ...options
  }

  return new Promise((resolve, reject) => {
    jwt.sign(payload, cert, jwtOptions, (err, token) => {
      if (err) {
        reject(err)
      }

      resolve(token)
    })
  })
}

const tryValidateJWT = (token, options) => {
  if (!token) {
    return Promise.reject(new Error('Token Not provided'))
  }

  const cert = fs.readFileSync(`${__dirname}${JWT_PUBLIC_KEY_PATH}`)
  const jwtOptions = {
    ...jwtGlobalOpts,
    ...options
  }

  return new Promise((resolve, reject) => {
    jwt.verify(token, cert, jwtOptions, (err) => {
      if (err) {
        reject(err)
      }

      resolve(token)
    })
  })
}

module.exports = {
  createJWT,
  tryValidateJWT
}
