const amqp = require('amqplib')
const { rabbit } = require('../configuration')
const {
  EXCHANGE_AUTH_ATTEMPTED,
  EXCHANGE_AUTH_VERIFIED,
  QEUE_AUTH_ATTEMPTED
} = require('../configuration/constants').communicationTopology
const { logger } = require('../utils/logger')
let rabbitChannel = null

const assertAuthAttemptChannelState = () => {
  return rabbitChannel.assertExchange(EXCHANGE_AUTH_ATTEMPTED, 'topic', { durable: true })
    .then(() => {
      return rabbitChannel.assertQueue(QEUE_AUTH_ATTEMPTED, { durable: true })
    })
    .then((result) => {
      return rabbitChannel.bindQueue(result.queue, EXCHANGE_AUTH_ATTEMPTED, '#')
    })
}

const openConnection = () => {
  return amqp
    .connect(rabbit.connStr)
    .then((conn) => {
      return conn.createChannel()
    })
    .then((channel) => {
      rabbitChannel = channel
      return assertAuthAttemptChannelState()
    })
    .catch((err) => {
      logger.log({
        level: 'error',
        message: `Failed connect to RabbitMQ, reconnecting... ${JSON.stringify(
          err
        )}`
      })
      return new Promise((resolve) => {
        setTimeout(() => resolve(openConnection()), rabbit.retryInterval)
      })
    })
}

const registerConsumer = (queue, consumer) => {
  return rabbitChannel.consume(queue, consumer)
}

const registerAuthAttemptConsumer = (consumer) => {
  return registerConsumer(QEUE_AUTH_ATTEMPTED, consumer)
}

const publishToExchange = (exchange, msg, routingKey) => {
  return rabbitChannel.publish(
    exchange,
    routingKey,
    Buffer.from(JSON.stringify(msg)),
    {
      persistent: true,
      contentType: 'application/json'
    }
  )
}

const publishToAuthVerifiedExchange = (msg, routingKey) => {
  return publishToExchange(EXCHANGE_AUTH_VERIFIED, msg, routingKey)
}

const ack = (message, allUpTo = false) => {
  rabbitChannel.ack(message, allUpTo)
}

const nack = (message, multiple = false, requeue = false) => {
  rabbitChannel.nack(message, multiple, requeue)
}

module.exports = {
  openConnection,
  registerAuthAttemptConsumer,
  publishToAuthVerifiedExchange,
  ack,
  nack
}
