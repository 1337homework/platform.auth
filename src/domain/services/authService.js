const _ = require('lodash')
const jwtHandler = require('../../jwt/jwtHandler')
const userRepository = require('../../repository/user')
const bcrypt = require('bcryptjs')
const rabbit = require('../../interfaces/rabbit')
const { logger } = require('../../utils/logger')
const {
  AUTH_VERIFIED_STATUS,
  AUTH_FAILED_STATUS,
  ANONYMOUS_SCOPE
} = require('../../configuration/constants').security

const getAuthenticatedUser = async (credentials) => {
  const user = await userRepository.findByUsername(credentials.username)

  return new Promise((resolve, reject) => {
    bcrypt.compare(credentials.password, user.password, (err, isMatch) => {
      if (err) {
        reject(err)
      }

      if (!isMatch) {
        reject(isMatch)
      }

      resolve(user)
    })
  })
}

const authenticate = async (message) => {
  if (
    message.requiredScopes.includes(ANONYMOUS_SCOPE)
  ) {
    const payload = {
      requiredScopes: [ANONYMOUS_SCOPE],
      nonce: message.nonce
    }

    const anonymousToken = await jwtHandler.createJWT(payload)

    return Promise.resolve({
      authStatus: AUTH_VERIFIED_STATUS,
      authToken: anonymousToken
    })
  }

  try {
    console.log(message)
    console.log(message.authToken)
    await jwtHandler.tryValidateJWT(message.authToken)
  } catch (e) {
    logger.log({ level: 'error', message: 'Token is invalid. Auth failed.' })
    return Promise.reject({
      authStatus: AUTH_FAILED_STATUS
    })
  }

  return Promise.resolve({
    authStatus: AUTH_VERIFIED_STATUS
  })
}

const login = async (message) => {
  let userWithScopes

  try {
    userWithScopes = await getAuthenticatedUser(message.logonPackage)
  } catch (e) {
    logger.log({ level: 'error', message: `Login failed: ${e}` })
    throw new Error(e)
  }

  try {
    const payload = {
      username: userWithScopes.username,
      scopes: _.map(userWithScopes.scopes, (el) => el.scope),
      nonce: message.nonce
    }

    const authToken = await jwtHandler.createJWT(payload)

    return Promise.resolve({
      authStatus: AUTH_VERIFIED_STATUS,
      authToken
    })
  } catch (e) {
    logger.log({ level: 'error', message: `Login failed: ${e}` })
    return Promise.reject({
      authStatus: AUTH_FAILED_STATUS
    })
  }

}

const authHandler = async (originalMessage) => {
  let authAttemptMessage

  logger.log({ level: 'info', message: 'Got session auth message.' })

  try {
    authAttemptMessage = JSON.parse(originalMessage.content.toString('utf8'))
  } catch (e) {
    logger.log({ level: 'error', message: 'Malformed auth message.' })
    // handle error report to error queue
    return
  }

  // logonPackage
  // authToken
  // requiredScopes
  // nonce
  // validationMessage
  let authVerifiedMessage = null
  try {
    if (authAttemptMessage.logonPackage) {
      authVerifiedMessage = await login(authAttemptMessage)
    } else {
      await authenticate(authAttemptMessage)
      authVerifiedMessage = authAttemptMessage
    }
  } catch (e) {
    logger.log({ level: 'info', message: `'Sesion is invalid. ${JSON.stringify(e)}` })
    // should handle error
  }

  logger.log({
    level: 'info',
    message: `Sending message to authorization service.`
  })

  rabbit.publishToAuthVerifiedExchange(authVerifiedMessage, originalMessage.fields.routingKey)
  rabbit.ack(originalMessage)
}

module.exports = {
  authHandler
}
