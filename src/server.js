require('./utils/env')
const rabbitmq = require('./interfaces/rabbit')
const db = require('./interfaces/sql')

const { authHandler } = require('./domain/services/authService')
const { logger } = require('./utils/logger')

Promise.all([rabbitmq.openConnection(), db.openConnection()])
  .then(() => {
    rabbitmq.registerAuthAttemptConsumer(authHandler)
    logger.log({
      level: 'info',
      message: 'Auth MS Connected to RabbitMQ. Listening for commands...'
    })
  })
  .catch((err) => {
    logger.log({ level: 'error', message: `${err.message} - ${err.stack}` })
    process.exit(1)
  })
