const { generalConfig } = require('../configuration')
const { createLogger, format, transports } = require('winston')

const { combine, timestamp, printf, prettyPrint } = format
const devLogFormat = printf((info) => `${info.timestamp} [${info.level}]: ${info.message}`)
const currentFormat = generalConfig.devLogger ? devLogFormat : prettyPrint()

const logger = createLogger({
  format: combine(
    timestamp(),
    currentFormat
  ),
  transports: [
    new transports.Console()
  ]
})

module.exports = {
  logger
}
