// Leaving like this just to test.
// Migration upgrades in real world
// is the main problem

const schema = (t) => {
  t.increments('id').primary()
}

exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.createTable('user_scopes', (table) => schema(table))
  ])
}

exports.down = function(knex) {
  return knex.schema.dropTableIfExists('user_scopes')
}
