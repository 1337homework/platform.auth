
const schema = (t) => {
  t.increments('id').primary()
  t.string('scope', 256)
}

exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.createTable('scopes', (table) => schema(table, knex))
  ])
}

exports.down = function(knex) {
  return knex.schema.dropTableIfExists('scopes')
}
