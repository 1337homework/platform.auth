// Just wanted to test knex migration upgrades
// looks a bit odd, in real world this would be pain,
// probably best would be to
// decouple queries from migrations and version them

const schema = (t) => {
  t.increments('id').primary()
  t.integer('scopeId')
  t.integer('userId')
}

exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.dropTableIfExists('user_scopes'),
    knex.schema.createTable('user_scopes', (table) => schema(table))
  ])
}

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.dropTableIfExists('user_scopes'),
    knex.schema.createTable('user_scopes', (table) => schema(table))
  ])
}
