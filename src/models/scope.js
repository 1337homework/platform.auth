const { Model } = require('objection')

class Scope extends Model {
  static get tableName() {
    return 'scopes'
  }

  static get jsonSchema() {
    return {
      type: 'object',
      required: ['id', 'scope'],

      properties: {
        id: { type: 'integer' },
        scope: { type: 'string', minLength: 1, maxLength: 255 }
      }
    }
  }

  static get relationMappings() {
    const User = require('./user')
    return {
      users: {
        relation: Model.ManyToManyRelation,
        modelClass: User,
        join: {
          from: 'scopes.id',
          through: {
            from: 'user_scopes.scopeId',
            to: 'user_scopes.userId'
          },
          to: 'users.id'
        }
      }
    }
  }
}

module.exports = Scope
