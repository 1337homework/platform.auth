const { Model } = require('objection')

class User extends Model {
  static get tableName() {
    return 'users'
  }

  static get jsonSchema() {
    return {
      type: 'object',
      required: ['username', 'password'],

      properties: {
        id: { type: 'integer' },
        username: { type: 'string', minLength: 1, maxLength: 255 },
        password: { type: 'string', minLength: 1, maxLength: 4095 }
      }
    }
  }

  static get relationMappings() {
    const Scope = require('./scope')

    return {
      scopes: {
        relation: Model.ManyToManyRelation,
        modelClass: Scope,
        join: {
          from: 'users.id',
          through: {
            from: 'user_scopes.userId',
            to: 'user_scopes.scopeId'
          },
          to: 'scopes.id'
        }
      }
    }
  }
}

module.exports = User
