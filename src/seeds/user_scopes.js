const bcrypt = require('bcryptjs')

exports.seed = function(knex, Promise) {
  const scopes = [
    { id: 1, scope: 'admin' },
    { id: 2, scope: 'user' },
    { id: 3, scope: 'anonymous' }
  ]

  const users = [
    { id: 1, username: 'demo', password: bcrypt.hashSync('demo', 10) },
    { id: 2, username: 'demo2', password: bcrypt.hashSync('demo2', 10) },
    { id: 3, username: 'admin', password: bcrypt.hashSync('admin', 10) }
  ]

  const userScopes = [
    { id: 1, scopeId: 1, userId: 3 },
    { id: 2, scopeId: 2, userId: 1 },
    { id: 3, scopeId: 2, userId: 2 },
    { id: 4, scopeId: 3, userId: 1 },
    { id: 5, scopeId: 3, userId: 2 },
    { id: 6, scopeId: 3, userId: 3 }
  ]

  return Promise
    .all([
      knex('users').del(),
      knex('scopes').del(),
      knex('user_scopes').del()
    ])
    .then(() => {
      return knex('users').insert(users)
    })
    .then(() => {
      return knex('scopes').insert(scopes)
    })
    .then(() => {
      return knex('user_scopes').insert(userScopes)
    })
}
