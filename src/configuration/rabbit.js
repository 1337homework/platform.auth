module.exports = {
  connStr: process.env.RABBIT_CONN_STR || 'amqp://guest:guest@rabbitmq/dev_vhost',
  retryInterval: process.env.RABBIT_RETRY_INTERVAL || 5000
}
