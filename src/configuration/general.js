module.exports = {
  devLogger: process.env.DEV_LOGGER || true,
  secretPassword: process.env.SECRET_PASSWORD || 'supersecretpassword'
}
